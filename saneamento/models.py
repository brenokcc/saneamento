# -*- coding: utf-8 -*-
from djangoplus.db import models
from djangoplus.decorators import attr, action, subset

ANO_CHOICES = [[2016, 2016], [2017, 2017], [2018, 2018]]


class Formulario001(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up001 = models.CharField(u'Nome de unidade', help_text=u'', null=True, blank=True)
    up003 = models.CharField(u'Tipo de unidade, segundo o município informante', help_text=u'', null=True, blank=True)
    upxxx = models.CharField(u'Situação da Classificação', help_text=u'', null=True, blank=True)
    up079 = models.CharField(u'Município responsável pelo gerenciamento', help_text=u'', null=True, blank=True)
    up002 = models.IntegerField(u'Início de operação', help_text=u'', null=True, blank=True, choices=ANO_CHOICES)
    up050 = models.CharField(u'Licença', help_text=u'', null=True, blank=True)
    up051 = models.CharField(u'Unidade em operação no ano de referência', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}),
                 (u'Dados Gerais', {'fields': ('up001', 'up003', 'upxxx', 'up079', 'up002', 'up050', 'up051'), }),)

    class Meta:
        verbose_name = u'Cadastro Nacional de Unidades de Processamento de Resíduos Sólidos Urbanos'
        verbose_name_plural = u'Cadastro Nacional de Unidades de Processamento de Resíduos Sólidos Urbanos'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (
        u'Tabela Up05 - Cadastro Nacional de Unidades de Processamento de Resíduos Sólidos Urbanos', self.ano)


class Formulario002(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    nome = models.CharField(u'Nome do orgão responsável pela gestão', help_text=u'', null=True, blank=True)
    sigla = models.CharField(u'Sigla', help_text=u'', null=True, blank=True)
    abrangencia = models.CharField(u'Abrangência', help_text=u'', null=True, blank=True)
    natureza_juridica = models.CharField(u'Natureza jurídica do órgão municipal responsável', help_text=u'', null=True,
                                         blank=True)
    tipo = models.CharField(u'Tipo de serviço', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}),
                 (u'Dados Gerais', {'fields': ('nome', 'sigla', 'abrangencia', 'natureza_juridica', 'tipo'), }),)

    class Meta:
        verbose_name = u'Orgão Responsável Pela Gestão'
        verbose_name_plural = u'Orgão Responsável Pela Gestão'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Orgão Responsável Pela Gestão', self.ano)


class Formulario003(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in001 = models.DecimalField(u'Taxa de empregados por habitante urbano', help_text=u'empreg./1000hab.', null=True,
                                blank=True)
    in002 = models.DecimalField(u'Despesa por empregado', help_text=u'R$/empregado', null=True, blank=True)
    in003 = models.DecimalField(u'Incidência de despesas com RSU na prefeitura', help_text=u'percentual (%)', null=True, blank=True)
    in004 = models.DecimalField(u'Incidência de despesas com  empresas contratadas', help_text=u'percentual (%)', null=True,
                                blank=True)
    in005 = models.DecimalField(u'Auto-suficiência  financeira', help_text=u'percentual (%)', null=True, blank=True)
    in006 = models.DecimalField(u'Despesas per capita com RSU', help_text=u'R$/habitante', null=True, blank=True)
    in007 = models.DecimalField(u'incidência de  empregados próprios', help_text=u'percentual (%)', null=True, blank=True)
    in008 = models.DecimalField(u'Incidência de empreg. de empr. contrat. no total de empreg. no manejo ',
                                help_text=u'percentual (%)', null=True, blank=True)
    in010 = models.DecimalField(u'Incidência de empreg. admin. no total de empreg no manejo', help_text=u'percentual (%)', null=True,
                                blank=True)
    in011 = models.DecimalField(u'Receita arrecadada per capita com serviços de manejo', help_text=u'R$/habitante',
                                null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {
        'fields': ('in001', 'in002', 'in003', 'in004', 'in005', 'in006', 'in007', 'in008', 'in010', 'in011'), }),)

    class Meta:
        verbose_name = u'Indicadores Gerais'
        verbose_name_plural = u'Indicadores Gerais'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Tabela In01 - Indicadores Gerais', self.ano)


class Formulario004(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in015 = models.DecimalField(u'Tx cobertura da coleta RDO em relação à pop. total', help_text=u'percentual (%)', null=True,
                                blank=True)
    in016 = models.DecimalField(u'Tx cobertura da coleta RDO em relação à pop. urbana', help_text=u'percentual (%)', null=True,
                                blank=True)
    in014 = models.DecimalField(u'Tx. cobertura de coleta direta RDO relativo à pop. urbana', help_text=u'percentual (%)', null=True,
                                blank=True)
    in017 = models.DecimalField(u'Taxa de terceirização da coleta', help_text=u'percentual (%)', null=True, blank=True)
    in018 = models.DecimalField(u'Produtividades média de coletadores e motorista', help_text=u'Kg/empregado  x dia',
                                null=True, blank=True)
    in019 = models.DecimalField(u'Taxa de motoristas e coletadores por habitante urbano', help_text=u'empreg./1000hab.',
                                null=True, blank=True)
    in021 = models.DecimalField(u'Massa [RDO+RPU] coletada per capita em relação à pop. urbana',
                                help_text=u'Kg/(hab.x dia)', null=True, blank=True)
    in022 = models.DecimalField(u'Massa RDO coletada per capita em relação à pop. total atendida',
                                help_text=u'Kg/(hab.x dia)', null=True, blank=True)
    in023 = models.DecimalField(u'Custo unitário da coleta', help_text=u'R$/tonelada', null=True, blank=True)
    in024 = models.DecimalField(u'Incidência do custo da coleta no custo total do manejo', help_text=u'percentual (%)', null=True,
                                blank=True)
    in025 = models.DecimalField(u'Incidência de emprega.da coleta no total de empregados no manejo', help_text=u'percentual (%)',
                                null=True, blank=True)
    in026 = models.DecimalField(u'Relação: quantidade RCD coletada pela Pref. p/quant. total  [RDO+RPU]',
                                help_text=u'percentual (%)', null=True, blank=True)
    in027 = models.DecimalField(u'Relação: quantidades coletadas de RPU por RDO', help_text=u'percentual (%)', null=True, blank=True)
    in028 = models.DecimalField(u'Massa [RDO+RPU] coletada per capita em relação à população total atendida ',
                                help_text=u'Kg/(hab.x dia)', null=True, blank=True)
    in029 = models.DecimalField(u'Massa de RCD per capita/ano em relação à pop. urbana', help_text=u'Kg/(hab.x ano)',
                                null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'in015', 'in016', 'in014', 'in017', 'in018', 'in019', 'in021', 'in022', 'in023', 'in024', 'in025', 'in026', 'in027',
    'in028', 'in029'), }),)

    class Meta:
        verbose_name = u'Indicadores Sobre Coleta de Resíduos Sólidos'
        verbose_name_plural = u'Indicadores Sobre Coleta de Resíduos Sólidos'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Tabela In02 - Indicadores Sobre Coleta de Resíduos Sólidos', self.ano)


class Formulario005(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in030 = models.DecimalField(u'Taxa de cobertura da col. Seletiva porta-a-porta em relação a pop. Urbana',
                                help_text=u'percentual (%)', null=True, blank=True)
    in031 = models.DecimalField(u'Taxa de recuperação de recicláveis em relação à quantidade de RDO e RPU',
                                help_text=u'percentual (%)', null=True, blank=True)
    in032 = models.DecimalField(u'Massa recuperada per capita', help_text=u'Kg/(hab. x ano)', null=True, blank=True)
    in053 = models.DecimalField(u'Relação entre quantidades da coleta seletiva e RDO', help_text=u'percentual (%)', null=True,
                                blank=True)
    in034 = models.DecimalField(u'Incid. de papel/papelão sobre total mat. recuperado', help_text=u'percentual (%)', null=True,
                                blank=True)
    in038 = models.DecimalField(u'Incid.de metais sobre total material recuperado', help_text=u'percentual (%)', null=True,
                                blank=True)
    in039 = models.DecimalField(u'Incid.de vidros sobre total de material recuperado', help_text=u'percentual (%)', null=True,
                                blank=True)
    in040 = models.DecimalField(u'Incidência de ''outros'' sobre total material recuperado', help_text=u'percentual (%)', null=True,
                                blank=True)
    in054 = models.DecimalField(u'Massa per capita recolhida via coleta seletiva', help_text=u'Kg/(hab. x ano)',
                                null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Dados Gerais', {'fields': ('in030', 'in031', 'in032', 'in053', 'in034', 'in038', 'in039', 'in040', 'in054'), }),)

    class Meta:
        verbose_name = u'Indicadores Sobre Coleta Seletiva de Resíduos Sólidos'
        verbose_name_plural = u'Indicadores Sobre Coleta Seletiva de Resíduos Sólidos'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Tabela In03 - Indicadores Sobre Coleta Seletiva de Resíduos Sólidos', self.ano)


class Formulario006(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in036 = models.DecimalField(u'Massa de RSS coletada per capita', help_text=u'Kg/(1000hab.  X dia)', null=True,
                                blank=True)
    in037 = models.DecimalField(u'Taxa de RSS sobre [RDO+RPU]', help_text=u'percentual (%)', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': ('in036', 'in037'), }),)

    class Meta:
        verbose_name = u'Indicadores Sobre Coleta de Res. Saúde'
        verbose_name_plural = u'Indicadores Sobre Coleta de Res. Saúde'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Tabela In04 - Indicadores Sobre Coleta de Res. Saúde', self.ano)


class Formulario007(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up001 = models.CharField(u'Nome da unidade', help_text=u'', null=True, blank=True)
    up003 = models.CharField(u'Tipo de unidade, segundo o município informante', help_text=u'', null=True, blank=True)
    up025 = models.CharField(u' Município de origem dos resíduos', help_text=u'', null=True, blank=True)
    up080 = models.DecimalField(u'Total ', help_text=u'', null=True, blank=True)
    up007 = models.DecimalField(u'Dom+Pub', help_text=u'tonelada', null=True, blank=True)
    up008 = models.DecimalField(u'Saúde', help_text=u'tonelada', null=True, blank=True)
    up009 = models.DecimalField(u'Indústria', help_text=u'tonelada', null=True, blank=True)
    up010 = models.DecimalField(u'Entulho', help_text=u'tonelada', null=True, blank=True)
    up067 = models.DecimalField(u'Podas', help_text=u'tonelada', null=True, blank=True)
    up011 = models.DecimalField(u'Outros', help_text=u'tonelada', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': ('up001', 'up003', 'up025'), }), (
    u'Quantidade de resíduos recebidos',
    {'fields': ('up080', 'up007', 'up008', 'up009', 'up010', 'up067', 'up011'), }),)

    class Meta:
        verbose_name = u'Informações Sobre O Fluxo de Resíduos Para As Unidades de Processamento'
        verbose_name_plural = u'Informações Sobre O Fluxo de Resíduos Para As Unidades de Processamento'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (
        u'TABELA Up02 - Informações Sobre O Fluxo de Resíduos Para As Unidades de Processamento', self.ano)


class Formulario008(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up001 = models.CharField(u'Nome da unidade', help_text=u'', null=True, blank=True)
    up003 = models.CharField(u'Tipo de unidade, segundo o município informante', help_text=u'', null=True, blank=True)
    up079 = models.CharField(u'Município responsável pelo gerenciamento', help_text=u'', null=True, blank=True)
    up004 = models.CharField(u'Operador', help_text=u'', null=True, blank=True)
    up002 = models.IntegerField(u'Início de operação', help_text=u'', null=True, blank=True, choices=ANO_CHOICES)
    up012 = models.NullBooleanField(u'Recebe de outros municípios', help_text=u'', null=True, blank=True)
    up051 = models.NullBooleanField(u'Unidade em operação no ano de referência', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}),
                 (u'Dados Gerais', {'fields': ('up001', 'up003', 'up079', 'up004', 'up002', 'up012', 'up051'), }),)

    class Meta:
        verbose_name = u'Informações Sobre As Unidades de Processamento'
        verbose_name_plural = u'Informações Sobre As Unidades de Processamento'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Tabela Up01 - Informações Sobre As Unidades de Processamento', self.ano)


class Formulario009(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up028 = models.NullBooleanField(u'Instalação administrativa', help_text=u'', null=True, blank=True)
    up029 = models.NullBooleanField(u'Impermeabilização da base', help_text=u'', null=True, blank=True)
    up030 = models.NullBooleanField(u'Freqüência da cobertura dos resíduos', help_text=u'', null=True, blank=True)
    up031 = models.NullBooleanField(u'Drenagem de gases', help_text=u'', null=True, blank=True)
    up052 = models.NullBooleanField(u'Aproveitamento dos gases', help_text=u'', null=True, blank=True)
    up054 = models.NullBooleanField(u'Drenagem de águas pluviais', help_text=u'', null=True, blank=True)
    up034 = models.NullBooleanField(u'Recirculação de chorume', help_text=u'', null=True, blank=True)
    up032 = models.NullBooleanField(u'Drenagem de chorume', help_text=u'', null=True, blank=True)
    up033 = models.NullBooleanField(u'Tratamento interno de chorume', help_text=u'', null=True, blank=True)
    up053 = models.NullBooleanField(u'Tratamento externo de chorume', help_text=u'', null=True, blank=True)
    up035 = models.NullBooleanField(u'Vigilância', help_text=u'', null=True, blank=True)
    up036 = models.NullBooleanField(u'Monitoramento ambiental', help_text=u'', null=True, blank=True)
    up037 = models.NullBooleanField(u'Queima a céu aberto ', help_text=u'', null=True, blank=True)
    up038 = models.NullBooleanField(u'Animais exceto aves', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'up028', 'up029', 'up030', 'up031', 'up052', 'up054', 'up034', 'up032', 'up033', 'up053', 'up035', 'up036', 'up037',
    'up038'), }),)

    class Meta:
        verbose_name = u'Informações Sobre Características das Unidades de Processamento Por Disposição no Solo'
        verbose_name_plural = u'Informações Sobre Características das Unidades de Processamento Por Disposição no Solo'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (
        u'Tabela Up03 - Informações Sobre Características das Unidades de Processamento Por Disposição no Solo',
        self.ano)


class Formulario010(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up028 = models.NullBooleanField(u'Instalação administrativa', help_text=u'', null=True, blank=True)
    up029 = models.NullBooleanField(u'Impermeabilização da base', help_text=u'', null=True, blank=True)
    up030 = models.NullBooleanField(u'Freqüência da cobertura dos resíduos', help_text=u'', null=True, blank=True)
    up031 = models.NullBooleanField(u'Drenagem de gases', help_text=u'', null=True, blank=True)
    up052 = models.NullBooleanField(u'Aproveitamento dos gases', help_text=u'', null=True, blank=True)
    up054 = models.NullBooleanField(u'Drenagem de águas pluviais', help_text=u'', null=True, blank=True)
    up034 = models.NullBooleanField(u'Recirculação de chorume', help_text=u'', null=True, blank=True)
    up032 = models.NullBooleanField(u'Drenagem de chorume', help_text=u'', null=True, blank=True)
    up033 = models.NullBooleanField(u'Tratamento interno de chorume', help_text=u'', null=True, blank=True)
    up053 = models.NullBooleanField(u'Tratamento externo de chorume', help_text=u'', null=True, blank=True)
    up035 = models.NullBooleanField(u'Vigilância', help_text=u'', null=True, blank=True)
    up036 = models.NullBooleanField(u'Monitoramento ambiental', help_text=u'', null=True, blank=True)
    up037 = models.NullBooleanField(u'Queima a céu aberto ', help_text=u'', null=True, blank=True)
    up038 = models.NullBooleanField(u'Animais exceto aves', help_text=u'', null=True, blank=True)
    up081 = models.NullBooleanField(u'Ocorrência de Catadores', help_text=u'', null=True, blank=True)
    up082 = models.IntegerField(u'Qtd de catadores até 14 anos', help_text=u'', null=True, blank=True)
    up083 = models.IntegerField(u'Qtd de catadores maior que 14 anos', help_text=u'', null=True, blank=True)
    up039 = models.IntegerField(u'Existência de moradias', help_text=u'', null=True, blank=True)
    up040 = models.IntegerField(u'Qtd de moradias', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'up028', 'up029', 'up030', 'up031', 'up052', 'up054', 'up034', 'up032', 'up033', 'up053', 'up035', 'up036', 'up037',
    'up038', 'up081', 'up082', 'up083', 'up039', 'up040'), }),)

    class Meta:
        verbose_name = u'Informações Sobre Características das Unidades de Processamento Por Disposição no Solo'
        verbose_name_plural = u'Informações Sobre Características das Unidades de Processamento Por Disposição no Solo'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (
        u'Tabela Up03 - Informações Sobre Características das Unidades de Processamento Por Disposição no Solo',
        self.ano)


class Formulario011(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    up015 = models.IntegerField(u'Trator de esteiras', help_text=u'unidade', null=True, blank=True)
    up016 = models.IntegerField(u'Retroescavadeira', help_text=u'unidade', null=True, blank=True)
    up017 = models.IntegerField(u'Pá carregadeira', help_text=u'unidade', null=True, blank=True)
    up018 = models.IntegerField(u'Caminhão basculante', help_text=u'unidade', null=True, blank=True)
    up071 = models.IntegerField(u'Caminhão pipa', help_text=u'unidade', null=True, blank=True)
    up069 = models.IntegerField(u'Trator com rolo compactador', help_text=u'unidade', null=True, blank=True)
    up019 = models.IntegerField(u'Outros', help_text=u'unidade', null=True, blank=True)
    up020 = models.IntegerField(u'Trator de esteiras', help_text=u'unidade', null=True, blank=True)
    up021 = models.IntegerField(u'Retroescavadeira', help_text=u'unidade', null=True, blank=True)
    up022 = models.IntegerField(u'Pá carregadeira', help_text=u'unidade', null=True, blank=True)
    up023 = models.IntegerField(u'Caminhão basculante', help_text=u'unidade', null=True, blank=True)
    up075 = models.IntegerField(u'Caminhão pipa', help_text=u'unidade', null=True, blank=True)
    up073 = models.IntegerField(u'Trator com rolo compactador', help_text=u'unidade', null=True, blank=True)
    up024 = models.IntegerField(u'Outros', help_text=u'unidade', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Quantidade de equipamentos públicos usados rotineiramente na unidade de disposição',
    {'fields': ('up015', 'up016', 'up017', 'up018', 'up071', 'up069', 'up019'), }), (
                 u'Quantidade de equipamentos privados usados rotineiramente na unidade de disposição',
                 {'fields': ('up020', 'up021', 'up022', 'up023', 'up075', 'up073', 'up024'), }),)

    class Meta:
        verbose_name = u'Informações Sobre a Quantidade de Equipamentos das Unidades de Processamento Por Disposição no Solo'
        verbose_name_plural = u'Informações Sobre a Quantidade de Equipamentos das Unidades de Processamento Por Disposição no Solo'
        list_menu = u'2. Resíduos Sólidos::%s' % verbose_name, 'fa-th'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (
        u'TABELA Up04 - Informações Sobre a Quantidade de Equipamentos das Unidades de Processamento Por Disposição no Solo',
        self.ano)


class Formulario012(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    nome = models.CharField(u'Nome do prestador de serviços', help_text=u'', null=True, blank=True)
    sigla = models.CharField(u'Sigla', help_text=u'', null=True, blank=True)
    abrangencia = models.CharField(u'Abrangência', help_text=u'', null=True, blank=True)
    natureza_juridica = models.CharField(u'Natureza jurídica do prestador de serviços', help_text=u'', null=True,
                                         blank=True)
    tipo_servico = models.CharField(u'Tipo do Serviço', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Dados Gerais', {'fields': ('nome', 'sigla', 'abrangencia', 'natureza_juridica', 'tipo_servico'), }),)

    class Meta:
        verbose_name = u'Prestador de Serviços'
        verbose_name_plural = u'Prestador de Serviços'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Prestador de Serviços', self.ano)


class Formulario013(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in003 = models.DecimalField(u'Despesa total com os serviços por m3 faturado', help_text=u'R$/m³', null=True,
                                blank=True)
    in026 = models.DecimalField(u'Despesa de exploração por m3 faturado', help_text=u'R$/m³', null=True, blank=True)
    in027 = models.DecimalField(u'Despesa de exploração por economia', help_text=u'R$/ano/econ.', null=True, blank=True)
    in004 = models.DecimalField(u'Tarifa média praticada', help_text=u'R$/m³', null=True, blank=True)
    in005 = models.DecimalField(u'Tarifa média de água', help_text=u'R$/m³', null=True, blank=True)
    in006 = models.DecimalField(u'Tarifa média de esgoto', help_text=u'R$/m³', null=True, blank=True)
    in012 = models.DecimalField(u'Indicador de desempenho financeiro', help_text=u'percentual', null=True, blank=True)
    in029 = models.DecimalField(u'Índice de evasão de receitas', help_text=u'percentual', null=True, blank=True)
    in007 = models.DecimalField(
        u'Incidencia da despesa de pessoal e de serviço de terceiros nas despesas totais com os serviços',
        help_text=u'percentual', null=True, blank=True)
    in008 = models.DecimalField(u'Despesa média anual por empregado', help_text=u'R$/empreg.', null=True, blank=True)
    in030 = models.DecimalField(u'Margem da despesa de exploração', help_text=u'percentual', null=True, blank=True)
    in031 = models.DecimalField(u'Margem da despesa com pessoal próprio', help_text=u'percentual', null=True,
                                blank=True)
    in032 = models.DecimalField(u'Margem da despesa com pessoal próprio  total (equivalente)', help_text=u'percentual',
                                null=True, blank=True)
    in033 = models.DecimalField(u'Margem do serviço da divida', help_text=u'percentual', null=True, blank=True)
    in034 = models.DecimalField(u'Margem das outras despesas de exploração', help_text=u'percentual', null=True,
                                blank=True)
    in035 = models.DecimalField(u'Participação da despesa com pessoal próprio nas despesas de exploração',
                                help_text=u'percentual', null=True, blank=True)
    in036 = models.DecimalField(u'Participação da despesa com pessoal total (equivalente) nas despesas de exploração',
                                help_text=u'percentual', null=True, blank=True)
    in037 = models.DecimalField(u'Participação da despesa com energia elétrica nas despesas de exploração',
                                help_text=u'percentual', null=True, blank=True)
    in038 = models.DecimalField(u'Participação da despesa com produtos químicos nas despesas de exploração',
                                help_text=u'percentual', null=True, blank=True)
    in039 = models.DecimalField(u'Participação das outras despesas na despesa de exploração', help_text=u'percentual',
                                null=True, blank=True)
    in040 = models.DecimalField(u'Participação da receita operacional direta de água na receita operacional total',
                                help_text=u'percentual', null=True, blank=True)
    in041 = models.DecimalField(u'Participação da receita operacional direta de esgoto na receita operacional total',
                                help_text=u'percentual', null=True, blank=True)
    in042 = models.DecimalField(u'Participação da receita operacional indireta na receita operacional total',
                                help_text=u'percentual', null=True, blank=True)
    in054 = models.IntegerField(u'Dias de faturamento comprometidos com contas a receber', help_text=u'dias', null=True,
                                blank=True)
    in018 = models.IntegerField(u'Quantidade equivalente de pessoal total', help_text=u'empregados', null=True,
                                blank=True)
    in019 = models.DecimalField(u'Índice de produtividade: economias ativas por pessoal total (equivalente)',
                                help_text=u'econ./empreg. eqv.', null=True, blank=True)
    in102 = models.DecimalField(u'Índice de produtividade de pessoal total (equivalente)',
                                help_text=u'ligações/empreg.', null=True, blank=True)
    in048 = models.DecimalField(u'Índice de produtividade: empregados próprios por 1.000 ligações de água + esgoto',
                                help_text=u'empreg./mil lig.', null=True, blank=True)
    in002 = models.DecimalField(u'Índice de produtividade: economias ativas por pessoal próprio',
                                help_text=u'econ./empreg.', null=True, blank=True)
    in045 = models.DecimalField(u'Índice de produtividade: empregados próprios por 1.000 ligações de água',
                                help_text=u'empreg./mil lig.', null=True, blank=True)
    in101 = models.DecimalField(u'Índice de suficiência de caixa', help_text=u'percentual', null=True, blank=True)
    in060 = models.DecimalField(u'Índice de despesas por consumo de energia elétrica nos sistemas de água e esgotos',
                                help_text=u'R$/ kWh', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'in003', 'in026', 'in027', 'in004', 'in005', 'in006', 'in012', 'in029', 'in007', 'in008', 'in030', 'in031', 'in032',
    'in033', 'in034', 'in035', 'in036', 'in037', 'in038', 'in039', 'in040', 'in041', 'in042', 'in054', 'in018', 'in019',
    'in102', 'in048', 'in002', 'in045', 'in101', 'in060'), }),)

    class Meta:
        verbose_name = u'Indicadores Econômico-Financeiros e Administrativos'
        verbose_name_plural = u'Indicadores Econômico-Financeiros e Administrativos'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Indicadores Econômico-Financeiros e Administrativos', self.ano)


class Formulario014(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in055 = models.DecimalField(u'Índice de atendimento total de água', help_text=u'percentual', null=True, blank=True)
    in023 = models.DecimalField(u'Índice de atendimento urbano de água', help_text=u'percentual', null=True, blank=True)
    in001 = models.DecimalField(u'Densidade de economias de água por ligação', help_text=u'econ./lig.', null=True,
                                blank=True)
    in043 = models.DecimalField(u'Participação das economias residenciais de água no total das economias de água',
                                help_text=u'percentual', null=True, blank=True)
    in011 = models.DecimalField(u'Índice de macromedição', help_text=u'percentual', null=True, blank=True)
    in009 = models.DecimalField(u'Índice de hidrometração', help_text=u'percentual', null=True, blank=True)
    in010 = models.DecimalField(u'Índice de micromedição relativo ao volume disponibilizado', help_text=u'percentual',
                                null=True, blank=True)
    in044 = models.DecimalField(u'Índice de micromedição relativo ao consumo', help_text=u'percentual', null=True,
                                blank=True)
    in057 = models.DecimalField(u'Índice de fluoretação de água', help_text=u'percentual', null=True, blank=True)
    in052 = models.DecimalField(u'Índice de consumo de água', help_text=u'percentual', null=True, blank=True)
    in025 = models.DecimalField(u'Volume de água disponibilizado por economia', help_text=u'm³/mês/econ', null=True,
                                blank=True)
    in053 = models.DecimalField(u'Consumo médio de água por economia', help_text=u'm³/mês/econ', null=True, blank=True)
    in014 = models.DecimalField(u'Consumo micromedido por economia', help_text=u'm³/mês/econ', null=True, blank=True)
    in017 = models.DecimalField(u'Consumo de água faturado por economia', help_text=u'm³/mês/econ', null=True,
                                blank=True)
    in022 = models.DecimalField(u'Consumo médio per Capita de água', help_text=u'l/hab.dia', null=True, blank=True)
    in058 = models.DecimalField(u'Índice de consumo de energia elétrica em sistemas de abastecimento de água',
                                help_text=u'kWh/m3', null=True, blank=True)
    in020 = models.DecimalField(u'Extensão da rede de água por ligação', help_text=u'm/lig.', null=True, blank=True)
    in028 = models.DecimalField(u'Índice de faturamento de água', help_text=u'percentual', null=True, blank=True)
    in013 = models.DecimalField(u'Índice de perdas faturamento', help_text=u'percentual', null=True, blank=True)
    in049 = models.DecimalField(u'Índice de perdas na distribuição', help_text=u'percentual', null=True, blank=True)
    in050 = models.DecimalField(u'Índice bruto de perdas lineares', help_text=u'm³/dia/km', null=True, blank=True)
    in051 = models.DecimalField(u'Índice de perdas por ligação', help_text=u'l/dia/lig.', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'in055', 'in023', 'in001', 'in043', 'in011', 'in009', 'in010', 'in044', 'in057', 'in052', 'in025', 'in053', 'in014',
    'in017', 'in022', 'in058', 'in020', 'in028', 'in013', 'in049', 'in050', 'in051'), }),)

    class Meta:
        verbose_name = u'Indicadores Operacionais - Água'
        verbose_name_plural = u'Indicadores Operacionais - Água'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Indicadores Operacionais - Água', self.ano)


class Formulario015(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in056 = models.DecimalField(u'Índice de atendimento total de esgoto referido aos municípios atendidos com água',
                                help_text=u'percentual', null=True, blank=True)
    in024 = models.DecimalField(u'Índice de atendimento urbano de esgoto referido aos municípios atendidos com água',
                                help_text=u'percentual', null=True, blank=True)
    in047 = models.DecimalField(u'Índice de atendimento urbano de esgoto referido aos municípios atendidos com esgoto',
                                help_text=u'percentual', null=True, blank=True)
    in015 = models.DecimalField(u'Índice de coleta de esgoto', help_text=u'percentual', null=True, blank=True)
    in016 = models.DecimalField(u'Índice de tratamento de esgoto', help_text=u'percentual', null=True, blank=True)
    in046 = models.DecimalField(u'Índice de esgoto tratado referido à água consumida', help_text=u'percentual',
                                null=True, blank=True)
    in021 = models.DecimalField(u'Extensão da rede de esgoto por ligação', help_text=u'm/lig.', null=True, blank=True)
    in059 = models.DecimalField(u'Índice de consumo de energia elétrica em sistemas de esgotamento sanitário',
                                help_text=u'kWh/m³', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Dados Gerais', {'fields': ('in056', 'in024', 'in047', 'in015', 'in016', 'in046', 'in021', 'in059'), }),)

    class Meta:
        verbose_name = u'Indicadores Operacionais - Esgoto'
        verbose_name_plural = u'Indicadores Operacionais - Esgoto'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Indicadores Operacionais - Esgoto', self.ano)


class Formulario016(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in071 = models.DecimalField(u'Economias atingidas por paralisações', help_text=u'econ./paralis.', null=True,
                                blank=True)
    in072 = models.DecimalField(u'Duração média das paralisações', help_text=u'horas/paralis.', null=True, blank=True)
    in073 = models.DecimalField(u'Economias atingidas por intermitências', help_text=u'econ./interrup.', null=True,
                                blank=True)
    in074 = models.DecimalField(u'Duração média das intermitências', help_text=u'horas/interrup.', null=True,
                                blank=True)
    in077 = models.DecimalField(u'Duração média dos reparos de extravasamentos de esgotos', help_text=u'horas/extrav.',
                                null=True, blank=True)
    in082 = models.DecimalField(u'Extravasamentos de esgotos por extensão de rede', help_text=u'extrav./km', null=True,
                                blank=True)
    in083 = models.DecimalField(u'Duração média dos serviços executados', help_text=u'hora/serviço', null=True,
                                blank=True)
    in079 = models.DecimalField(u'Índice de conformidade da quantidade de amostra - Cloro Residual',
                                help_text=u'percentual', null=True, blank=True)
    in075 = models.DecimalField(u'Incidência das análises de cloro residual fora do padrão', help_text=u'percentual',
                                null=True, blank=True)
    in080 = models.DecimalField(u'Índice de conformidade da quantidade de amostra - Turbidez', help_text=u'percentual',
                                null=True, blank=True)
    in076 = models.DecimalField(u'Incidência das análises de turbidez fora do padrão', help_text=u'percentual',
                                null=True, blank=True)
    in085 = models.DecimalField(u'Índice de conformidade da quantidade de amostra - Coliformes Totais',
                                help_text=u'percentual', null=True, blank=True)
    in084 = models.DecimalField(u'Incidência das análises de coliformes totais fora do padrão', help_text=u'percentual',
                                null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'in071', 'in072', 'in073', 'in074', 'in077', 'in082', 'in083', 'in079', 'in075', 'in080', 'in076', 'in085',
    'in084'), }),)

    class Meta:
        verbose_name = u'Indicadores Sobre Qualidade'
        verbose_name_plural = u'Indicadores Sobre Qualidade'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = False
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Indicadores Sobre Qualidade', self.ano)


class Formulario017(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    in061 = models.DecimalField(u'Liquidez corrente', help_text=u'', null=True, blank=True)
    in062 = models.DecimalField(u'Liquidez geral', help_text=u'', null=True, blank=True)
    in063 = models.DecimalField(u'Grau de endividamento', help_text=u'', null=True, blank=True)
    in064 = models.DecimalField(u'Margem operacional com depreciação', help_text=u'percentual', null=True, blank=True)
    in068 = models.DecimalField(u'Margem operacional sem depreciação', help_text=u'percentual', null=True, blank=True)
    in065 = models.DecimalField(u'Margem líquida com depreciação', help_text=u'percentual', null=True, blank=True)
    in069 = models.DecimalField(u'Margem líquida sem depreciação', help_text=u'percentual', null=True, blank=True)
    in066 = models.DecimalField(u'Retorno sobre o patrimônio líquido', help_text=u'percentual', null=True, blank=True)
    in067 = models.DecimalField(u'Composição de exigibilidades', help_text=u'percentual', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Dados Gerais', {'fields': ('in061', 'in062', 'in063', 'in064', 'in068', 'in065', 'in069', 'in066', 'in067'), }),)

    class Meta:
        verbose_name = u'Indicadores de Balanço Contábil'
        verbose_name_plural = u'Indicadores de Balanço Contábil'
        list_menu = u'1. Água e Esgoto::%s' % verbose_name, 'fa-tint'
        list_shortcut = True
        can_list = u'Gestor'
        can_edit = u'Gestor'

    def __unicode__(self):
        return u'%s - [%s]' % (u'Indicadores de Balanço Contábil', self.ano)


class Formulario018(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    nome_scretaria = models.CharField(
        u'Nome da Secretaria ou Setor responsável pelos serviços de Drenagem e Manejo das Águas Pluviais Urbanas no município',
        help_text=u'', null=True, blank=True)
    regiao_hidrografica = models.CharField(u'Região Hidrográfica em que se encontra o município (Fonte: ANA):',
                                           help_text=u'', null=True, blank=True)
    bacias_hidrograficas = models.CharField(
        u'Nome da(s) bacia(s) hidrográfica(s) a que pertence o município (Fonte: ANA):', help_text=u'', null=True,
        blank=True)
    comite_bacias = models.CharField(u'Comitê de Bacia ou de Sub-bacia Hidrográfica', help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (
    u'Dados Gerais', {'fields': ('nome_scretaria', 'regiao_hidrografica', 'bacias_hidrograficas', 'comite_bacias'), }),)

    class Meta:
        verbose_name = u'Dados Gerais'
        verbose_name_plural = u'Dados Gerais'
        list_menu = u'3. Drenagem::%s' % verbose_name, 'fa-random'
        can_list = u'Gestor'
        can_edit = u'Gestor'
        list_shortcut = True

    def __unicode__(self):
        return u'%s - [%s]' % (u'Dados Gerais', self.ano)


class Formulario019(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    forma_cobranca = models.CharField(
        u'Forma de cobrança ou de ônus indireto pelo uso ou disposição dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
        help_text=u'', null=True, blank=True)
    qtd_unidades = models.IntegerField(
        u'Quantidade total de unidades edificadas urbanas tributadas com taxa específica dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
        help_text=u'', null=True, blank=True)
    valor_taxa = models.DecimalField(
        u'Valor da taxa específica dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas por unidade edificada urbana',
        help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}),
                 (u'Dados Gerais', {'fields': ('forma_cobranca', 'qtd_unidades', 'valor_taxa'), }),)

    class Meta:
        verbose_name = u'Dados da Cobrança'
        verbose_name_plural = u'Dados da Cobrança'
        list_menu = u'3. Drenagem::%s' % verbose_name, 'fa-random'
        can_list = u'Gestor'
        can_edit = u'Gestor'
        list_shortcut = True

    def __unicode__(self):
        return u'%s - [%s]' % (u'Dados da Cobrança', self.ano)


class Formulario020(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    qtd_pessoal_proprio = models.IntegerField(
        u'Quantidade de pessoal próprio alocado nos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
        help_text=u'', null=True, blank=True)
    qtd_pessoal_tercerizado = models.IntegerField(
        u'Quantidade de pessoal terceirizado alocado nos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
        help_text=u'', null=True, blank=True)
    forma_custeio = models.CharField(u'Formas de custeio dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
                                     help_text=u'', null=True, blank=True)
    receita_total = models.DecimalField(u'Receita total dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
                                        help_text=u'', null=True, blank=True)
    despesa_exploracao_direta = models.DecimalField(u'Despesas de Exploração (DEX) diretas ou de custeio totais dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas',
        help_text=u'', null=True, blank=True)
    desenbolso_proprio = models.CharField(
        u'Desembolsos de investimentos com recursos próprios em Drenagem e Manejo das Águas Pluviais Urbanas ',
        help_text=u'', null=True, blank=True)
    investimento_recursos_onerosos = models.DecimalField(
        u'Investimentos com recursos onerosos em Drenagem e Manejo das Águas Pluviais Urbanas', help_text=u'',
        null=True, blank=True)
    investimento_recursos_nao_onerosos = models.DecimalField(
        u'Investimentos com recursos não onerosos em Drenagem e Manejo das Águas Pluviais Urbanas', help_text=u'',
        null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'qtd_pessoal_proprio', 'qtd_pessoal_tercerizado', 'forma_custeio', 'receita_total', 'despesa_exploracao_direta',
    'desenbolso_proprio', 'investimento_recursos_onerosos', 'investimento_recursos_nao_onerosos'), }),)

    class Meta:
        verbose_name = u'Dados Financeiros'
        verbose_name_plural = u'Dados Financeiros'
        list_menu = u'3. Drenagem::%s' % verbose_name, 'fa-random'
        can_list = u'Gestor'
        can_edit = u'Gestor'
        list_shortcut = True

    def __unicode__(self):
        return u'%s - [%s]' % (u'Dados Financeiros', self.ano)


class Formulario021(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    tipo_sistema = models.CharField(u'Tipo de sistema de Drenagem Urbana', help_text=u'', null=True, blank=True)
    extensao_total = models.DecimalField(u'Extensão total de vias públicas urbanas do município', help_text=u'Km',
                                         null=True, blank=True)
    extensao_total_pavimentada = models.DecimalField(
        u'Extensão total de vias públicas urbanas com pavimento e meio-fio', help_text=u'Km', null=True, blank=True)
    qtd_bocas_lobo = models.IntegerField(u'Quantidade de bocas de lobo existentes no município', help_text=u'',
                                         null=True, blank=True)
    qtd_pocos = models.IntegerField(u'Quantidade de poços de visita (PV) existentes no município', help_text=u'',
                                    null=True, blank=True)
    extensao_vias_publicas = models.DecimalField(
        u'Extensão total de vias públicas urbanas com redes ou canais de águas pluviais subterrâneos', help_text=u'Km',
        null=True, blank=True)
    extensao_cursos_de_agua = models.DecimalField(u'Extensão total dos cursos d\'água naturais em áreas urbanas', help_text=u'Km', null=True, blank=True)
    extensao_parques_lineares = models.DecimalField(u'Extensão total de parques lineares ao longo de cursos d’água',
                                                    help_text=u'Km', null=True, blank=True)
    area_parques_lineares = models.DecimalField(u'área de Parques lineares', help_text=u'Km2', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'tipo_sistema', 'extensao_total', 'extensao_total_pavimentada', 'qtd_bocas_lobo', 'qtd_pocos',
    'extensao_vias_publicas', 'extensao_cursos_de_agua', 'extensao_parques_lineares', 'area_parques_lineares'), }),)

    class Meta:
        verbose_name = u'Dados de Infraestrutura'
        verbose_name_plural = u'Dados de Infraestrutura'
        list_menu = u'3. Drenagem::%s' % verbose_name, 'fa-random'
        can_list = u'Gestor'
        can_edit = u'Gestor'
        list_shortcut = True

    def __unicode__(self):
        return u'%s - [%s]' % (u'Dados de Infraestrutura', self.ano)


class Formulario022(models.Model):
    ano = models.IntegerField(u'Ano', choices=ANO_CHOICES)
    qtd_metros_limpesa = models.IntegerField(u'Limpeza em metro linear das estruturas de drenagem do Município',
                                             help_text=u'', null=True, blank=True)
    existe_instrumento_controle = models.NullBooleanField(
        u'Existem instrumentos de controle e monitoramento hidrológicos', help_text=u'', null=True, blank=True)
    existe_sistema_alerta = models.NullBooleanField(u'Existem sistemas de alerta de riscos hidrológicos', help_text=u'',
                                                    null=True, blank=True)
    existe_mapeamento_risco = models.NullBooleanField(u'Existe mapeamento de áreas de risco de inundação dos cursos d\'água urbanos', help_text=u'', null=True, blank=True)
    qtd_domicilios_risco_iinundacao = models.IntegerField(u'Quantidade de domicílios sujeitos a risco de inundação',
                                                          help_text=u'', null=True, blank=True)
    numero_alagamentos = models.IntegerField(u'Número de alagamentos na área urbana do município ', help_text=u'',
                                             null=True, blank=True)
    numero_unidades_edificadas = models.IntegerField(
        u'Número de unidades edificadas atingidas na área urbana do município devido a eventos hidrológicos impactantes',
        help_text=u'', null=True, blank=True)

    fieldsets = ((u'Exercício', {'fields': ('ano',)}), (u'Dados Gerais', {'fields': (
    'qtd_metros_limpesa', 'existe_instrumento_controle', 'existe_sistema_alerta', 'existe_mapeamento_risco',
    'qtd_domicilios_risco_iinundacao', 'numero_alagamentos', 'numero_unidades_edificadas'), }),)

    class Meta:
        verbose_name = u'Dados Operacionais'
        verbose_name_plural = u'Dados Operacionais'
        list_menu = u'3. Drenagem::%s' % verbose_name, 'fa-random'
        can_list = u'Gestor'
        can_edit = u'Gestor'
        list_shortcut = True

    def __unicode__(self):
        return u'%s - [%s]' % (u'Dados Operacionais', self.ano)




