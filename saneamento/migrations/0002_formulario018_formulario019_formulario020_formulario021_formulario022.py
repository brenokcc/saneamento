# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-25 16:32
from __future__ import unicode_literals

from django.db import migrations, models
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('saneamento', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Formulario018',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', djangoplus.db.models.fields.IntegerField(choices=[[2016, 2016], [2017, 2017], [2018, 2018]], verbose_name='Ano')),
                ('nome_scretaria', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Nome da Secretaria ou Setor respons\xe1vel pelos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas no munic\xedpio')),
                ('regiao_hidrografica', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Regi\xe3o Hidrogr\xe1fica em que se encontra o munic\xedpio (Fonte: ANA):')),
                ('bacias_hidrograficas', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Nome da(s) bacia(s) hidrogr\xe1fica(s) a que pertence o munic\xedpio (Fonte: ANA):')),
                ('comite_bacias', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Comit\xea de Bacia ou de Sub-bacia Hidrogr\xe1fica')),
            ],
            options={
                'verbose_name': 'Dados Gerais',
                'verbose_name_plural': 'Dados Gerais',
            },
        ),
        migrations.CreateModel(
            name='Formulario019',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', djangoplus.db.models.fields.IntegerField(choices=[[2016, 2016], [2017, 2017], [2018, 2018]], verbose_name='Ano')),
                ('forma_cobranca', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Forma de cobran\xe7a ou de \xf4nus indireto pelo uso ou disposi\xe7\xe3o dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('qtd_unidades', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade total de unidades edificadas urbanas tributadas com taxa espec\xedfica dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('valor_taxa', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Valor da taxa espec\xedfica dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas por unidade edificada urbana')),
            ],
            options={
                'verbose_name': 'Dados da Cobran\xe7a',
                'verbose_name_plural': 'Dados da Cobran\xe7a',
            },
        ),
        migrations.CreateModel(
            name='Formulario020',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', djangoplus.db.models.fields.IntegerField(choices=[[2016, 2016], [2017, 2017], [2018, 2018]], verbose_name='Ano')),
                ('qtd_pessoal_proprio', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade de pessoal pr\xf3prio alocado nos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('qtd_pessoal_tercerizado', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade de pessoal terceirizado alocado nos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('forma_custeio', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Formas de custeio dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('receita_total', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Receita total dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('despesa_exploracao_direta', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Despesas de Explora\xe7\xe3o (DEX) diretas ou de custeio totais dos servi\xe7os de Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('desenbolso_proprio', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Desembolsos de investimentos com recursos pr\xf3prios em Drenagem e Manejo das \xc1guas Pluviais Urbanas ')),
                ('investimento_recursos_onerosos', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Investimentos com recursos onerosos em Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
                ('investimento_recursos_nao_onerosos', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Investimentos com recursos n\xe3o onerosos em Drenagem e Manejo das \xc1guas Pluviais Urbanas')),
            ],
            options={
                'verbose_name': 'Dados Financeiros',
                'verbose_name_plural': 'Dados Financeiros',
            },
        ),
        migrations.CreateModel(
            name='Formulario021',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', djangoplus.db.models.fields.IntegerField(choices=[[2016, 2016], [2017, 2017], [2018, 2018]], verbose_name='Ano')),
                ('tipo_sistema', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Tipo de sistema de Drenagem Urbana')),
                ('extensao_total', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km', max_digits=9, null=True, verbose_name='Extens\xe3o total de vias p\xfablicas urbanas do munic\xedpio')),
                ('extensao_total_pavimentada', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km', max_digits=9, null=True, verbose_name='Extens\xe3o total de vias p\xfablicas urbanas com pavimento e meio-fio')),
                ('qtd_bocas_lobo', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade de bocas de lobo existentes no munic\xedpio')),
                ('qtd_pocos', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade de po\xe7os de visita (PV) existentes no munic\xedpio')),
                ('extensao_vias_publicas', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km', max_digits=9, null=True, verbose_name='Extens\xe3o total de vias p\xfablicas urbanas com redes ou canais de \xe1guas pluviais subterr\xe2neos')),
                ('extensao_cursos_de_agua', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km', max_digits=9, null=True, verbose_name="Extens\xe3o total dos cursos d'\xe1gua naturais em \xe1reas urbanas")),
                ('extensao_parques_lineares', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km', max_digits=9, null=True, verbose_name='Extens\xe3o total de parques lineares ao longo de cursos d\u2019\xe1gua')),
                ('area_parques_lineares', djangoplus.db.models.fields.DecimalField(blank=True, decimal_places=2, help_text='Km2', max_digits=9, null=True, verbose_name='\xe1rea de Parques lineares')),
            ],
            options={
                'verbose_name': 'Dados de Infraestrutura',
                'verbose_name_plural': 'Dados de Infraestrutura',
            },
        ),
        migrations.CreateModel(
            name='Formulario022',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', djangoplus.db.models.fields.IntegerField(choices=[[2016, 2016], [2017, 2017], [2018, 2018]], verbose_name='Ano')),
                ('qtd_metros_limpesa', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Limpeza em metro linear das estruturas de drenagem do Munic\xedpio')),
                ('existe_instrumento_controle', djangoplus.db.models.fields.NullBooleanField(verbose_name='Existem instrumentos de controle e monitoramento hidrol\xf3gicos')),
                ('existe_sistema_alerta', djangoplus.db.models.fields.NullBooleanField(verbose_name='Existem sistemas de alerta de riscos hidrol\xf3gicos')),
                ('existe_mapeamento_risco', djangoplus.db.models.fields.NullBooleanField(verbose_name="Existe mapeamento de \xe1reas de risco de inunda\xe7\xe3o dos cursos d'\xe1gua urbanos")),
                ('qtd_domicilios_risco_iinundacao', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='Quantidade de domic\xedlios sujeitos a risco de inunda\xe7\xe3o')),
                ('numero_alagamentos', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='N\xfamero de alagamentos na \xe1rea urbana do munic\xedpio ')),
                ('numero_unidades_edificadas', djangoplus.db.models.fields.IntegerField(blank=True, null=True, verbose_name='N\xfamero de unidades edificadas atingidas na \xe1rea urbana do munic\xedpio devido a eventos hidrol\xf3gicos impactantes')),
            ],
            options={
                'verbose_name': 'Dados Operacionais',
                'verbose_name_plural': 'Dados Operacionais',
            },
        ),
    ]
