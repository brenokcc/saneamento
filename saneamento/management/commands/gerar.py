# -*- coding: utf-8 -*-
import os, re

from django.template.loader import render_to_string
from dropbox import Dropbox
from django.conf import settings
from dropbox.files import WriteMode
from django.core.management.base import BaseCommand


def normalizar(nome):
    mome = nome.replace(u'Ó', u'ó').replace(u'Ç', u'ç').replace(u'Ã', u'ã').replace(u'Õ', u'õ')
    ##
    # Normaliza o nome próprio dado, aplicando a capitalização correta de acordo
    # com as regras e exceções definidas no código.
    ##
    ponto = '\.'
    ponto_espaco = '. '
    espaco = ' '
    regex_multiplos_espacos = '\s+'
    regex_numero_romano = '^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'

    nome = re.sub(ponto, ponto_espaco, nome) #colocando espaço após nomes abreviados
    nome = re.sub(regex_multiplos_espacos, espaco, nome) #retirando espaços múltiplos
    nome = nome.title() #alterando os nomes para CamelCase
    partes_nome = nome.split(espaco) #separando as palavras numa lista
    excecoes = [
                'de', 'di', 'do', 'da', 'dos', 'das', 'dello', 'della', 'dalla',
                'dal', 'del', 'e', 'em', 'na', 'no', 'nas', 'nos', 'van', 'von', 'y', 'a',
    ]

    resultado = []

    for palavra in partes_nome:
        if palavra.lower() in excecoes:
            resultado.append(palavra.lower())
        elif re.match(regex_numero_romano, palavra.upper()):
            resultado.append(palavra.upper())
        else:
            resultado.append(palavra)

    nome = espaco.join(resultado)
    return nome

s = u'''TABELA Up05 - CADASTRO NACIONAL DE UNIDADES DE PROCESSAMENTO DE RESÍDUOS SÓLIDOS URBANOS
Nome de unidade	Tipo de unidade, segundo o município informante	Situação da Classificação	Município responsável pelo gerenciamento	Início de operação	Licença	Unidade em operação no ano de referência

Up001	Up003	Upxxx	Up079	Up002	Up050	Up051
CharField	CharField	CharField	CharField	AnoField	CharField	CharField	NullBooleanField


ORGÃO RESPONSÁVEL PELA GESTÃO
Nome do orgão responsável pela gestão	Sigla	Abrangência	Natureza jurídica do órgão municipal responsável	Tipo de serviço

nome	sigla	abrangencia	natureza_juridica	tipo
CharField	CharField	CharField	CharField	CharField


TABELA In01 - INDICADORES GERAIS
Taxa de empregados por habitante urbano	Despesa por empregado	Incidência de despesas com RSU na prefeitura	Incidência de despesas com  empresas contratadas	Auto-suficiência  financeira	Despesas per capita com RSU	incidência de  empregados próprios	Incidência de empreg. de empr. contrat. no total de empreg. no manejo 	Incidência de empreg. admin. no total de empreg no manejo	Receita arrecadada per capita com serviços de manejo
empreg./1000hab.	R$/empregado	%	%	%	R$/habitante	%	%	%	R$/habitante
IN001	IN002	IN003	IN004	IN005	IN006	IN007	IN008	IN010	IN011
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


TABELA In02 - INDICADORES SOBRE COLETA DE RESÍDUOS SÓLIDOS
Tx cobertura da coleta RDO em relação à pop. total	Tx cobertura da coleta RDO em relação à pop. urbana	Tx. cobertura de coleta direta RDO relativo à pop. urbana	Taxa de terceirização da coleta	Produtividades média de coletadores e motorista	Taxa de motoristas e coletadores por habitante urbano	Massa [RDO+RPU] coletada per capita em relação à pop. urbana	Massa RDO coletada per capita em relação à pop. total atendida	Custo unitário da coleta	Incidência do custo da coleta no custo total do manejo	Incidência de emprega.da coleta no total de empregados no manejo	Relação: quantidade RCD coletada pela Pref. p/quant. total  [RDO+RPU]	Relação: quantidades coletadas de RPU por RDO	Massa [RDO+RPU] coletada per capita em relação à população total atendida 	Massa de RCD per capita/ano em relação à pop. urbana
%	%	%	%	Kg/empregado  x dia	empreg./1000hab.	Kg/(hab.x dia)	Kg/(hab.x dia)	R$/tonelada	%	%	%	%	Kg/(hab.x dia)	Kg/(hab.x ano)
IN015	IN016	IN014	IN017	IN018	IN019	IN021	IN022	IN023	IN024	IN025	IN026	IN027	IN028	IN029
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


TABELA In03 - INDICADORES SOBRE COLETA SELETIVA  DE RESÍDUOS SÓLIDOS
Taxa de cobertura da col. Seletiva porta-a-porta em relação a pop. Urbana	Taxa de recuperação de recicláveis em relação à quantidade de RDO e RPU	Massa recuperada per capita	Relação entre quantidades da coleta seletiva e RDO	Incid. de papel/papelão sobre total mat. recuperado	Incid.de metais sobre total material recuperado	Incid.de vidros sobre total de material recuperado	Incidência de ''outros'' sobre total material recuperado	Massa per capita recolhida via coleta seletiva
%	%	Kg/(hab. x ano)	%	%	%	%	%	Kg/(hab. x ano)
IN030	IN031	IN032	IN053	IN034	IN038	IN039	IN040	IN054
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


TABELA In04 - INDICADORES SOBRE COLETA DE RES. SAÚDE
Massa de RSS coletada per capita	Taxa de RSS sobre [RDO+RPU]
Kg/(1000hab.  X dia)	%
IN036	IN037
DecimalField	DecimalField


TABELA Up02 - INFORMAÇÕES SOBRE  O FLUXO DE RESÍDUOS PARA AS UNIDADES DE PROCESSAMENTO
Dados Gerais
Nome da unidade	Tipo de unidade, segundo o município informante	 Município de origem dos resíduos
\t\t\t
Up001	Up003	Up025
CharField	CharField	CharField
###
Quantidade de resíduos recebidos
Total 	Dom+Pub	Saúde	Indústria	Entulho	Podas	Outros
tonelada	tonelada	tonelada	tonelada	tonelada	tonelada	tonelada
Up080	Up007	Up008	Up009	Up010	Up067	Up011
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


TABELA Up01 - INFORMAÇÕES SOBRE AS UNIDADES DE PROCESSAMENTO
Nome da unidade	Tipo de unidade, segundo o município informante	Município responsável pelo gerenciamento	Operador	Início de operação	Recebe de outros municípios	Unidade em operação no ano de referência

Up001	Up003	Up079	Up004	Up002	Up012	Up051
CharField	CharField	CharField	CharField	AnoField	NullBooleanField	NullBooleanField


TABELA Up03 - INFORMAÇÕES SOBRE  CARACTERÍSTICAS DAS UNIDADES DE PROCESSAMENTO POR DISPOSIÇÃO NO SOLO
Instalação administrativa	Impermeabilização da base	Freqüência da cobertura dos resíduos	Drenagem de gases	Aproveitamento dos gases	Drenagem de águas pluviais	Recirculação de chorume	Drenagem de chorume	Tratamento interno de chorume	Tratamento externo de chorume	Vigilância	Monitoramento ambiental	Queima a céu aberto 	Animais exceto aves

Up028	Up029	Up030	Up031	Up052	Up054	Up034	Up032	Up033	Up053	Up035	Up036	Up037	Up038
BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField	BooleanField


TABELA Up03 - INFORMAÇÕES SOBRE  CARACTERÍSTICAS DAS UNIDADES DE PROCESSAMENTO POR DISPOSIÇÃO NO SOLO
Instalação administrativa	Impermeabilização da base	Freqüência da cobertura dos resíduos	Drenagem de gases	Aproveitamento dos gases	Drenagem de águas pluviais	Recirculação de chorume	Drenagem de chorume	Tratamento interno de chorume	Tratamento externo de chorume	Vigilância	Monitoramento ambiental	Queima a céu aberto 	Animais exceto aves	Ocorrência de Catadores	Qtd de catadores até 14 anos	Qtd de catadores maior que 14 anos	Existência de moradias	Qtd de moradias

Up028	Up029	Up030	Up031	Up052	Up054	Up034	Up032	Up033	Up053	Up035	Up036	Up037	Up038	Up081	Up082	Up083	Up039	Up040
NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	NullBooleanField	IntegerField	IntegerField	IntegerField	IntegerField


TABELA Up04 - INFORMAÇÕES SOBRE A QUANTIDADE DE EQUIPAMENTOS DAS UNIDADES DE PROCESSAMENTO POR DISPOSIÇÃO NO SOLO
Quantidade de equipamentos públicos usados rotineiramente na unidade de disposição
Trator de esteiras	Retroescavadeira	Pá carregadeira	Caminhão basculante	Caminhão pipa	Trator com rolo compactador	Outros
unidade	unidade	unidade	unidade	unidade	unidade	unidade
Up015	Up016	Up017	Up018	Up071	Up069	Up019
IntegerField	IntegerField	IntegerField	IntegerField	IntegerField	IntegerField	IntegerField
###
Quantidade de equipamentos privados usados rotineiramente na unidade de disposição
Trator de esteiras	Retroescavadeira	Pá carregadeira	Caminhão basculante	Caminhão pipa	Trator com rolo compactador	Outros
unidade	unidade	unidade	unidade	unidade	unidade	unidade
Up020	Up021	Up022	Up023	Up075	Up073	Up024
IntegerField	IntegerField	IntegerField	IntegerField	IntegerField	IntegerField	IntegerField


PRESTADOR DE SERVIÇOS
Nome do prestador de serviços	Sigla	Abrangência	Natureza jurídica do prestador de serviços	Tipo do Serviço

nome	sigla	abrangencia	natureza_juridica	tipo_servico
CharField	CharField	CharField	CharField	CharField


INDICADORES ECONÔMICO-FINANCEIROS E ADMINISTRATIVOS
Despesa total com os serviços por m3 faturado	Despesa de exploração por m3 faturado	Despesa de exploração por economia	Tarifa média praticada	Tarifa média de água	Tarifa média de esgoto	Indicador de desempenho financeiro	Índice de evasão de receitas	Incidencia da despesa de pessoal e de serviço de terceiros nas despesas totais com os serviços	Despesa média anual por empregado	Margem da despesa de exploração	Margem da despesa com pessoal próprio	Margem da despesa com pessoal próprio  total (equivalente)	Margem do serviço da divida	Margem das outras despesas de exploração	Participação da despesa com pessoal próprio nas despesas de exploração	Participação da despesa com pessoal total (equivalente) nas despesas de exploração	Participação da despesa com energia elétrica nas despesas de exploração	Participação da despesa com produtos químicos nas despesas de exploração	Participação das outras despesas na despesa de exploração	Participação da receita operacional direta de água na receita operacional total	Participação da receita operacional direta de esgoto na receita operacional total	Participação da receita operacional indireta na receita operacional total	Dias de faturamento comprometidos com contas a receber	Quantidade equivalente de pessoal total	Índice de produtividade: economias ativas por pessoal total (equivalente)	Índice de produtividade de pessoal total (equivalente)	Índice de produtividade: empregados próprios por 1.000 ligações de água + esgoto	Índice de produtividade: economias ativas por pessoal próprio	Índice de produtividade: empregados próprios por 1.000 ligações de água	Índice de suficiência de caixa	Índice de despesas por consumo de energia elétrica nos sistemas de água e esgotos
R$/m³	R$/m³	R$/ano/econ.	R$/m³	R$/m³	R$/m³	percentual	percentual	percentual	R$/empreg.	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	percentual	dias	empregados	econ./empreg. eqv.	ligações/empreg.	empreg./mil lig.	econ./empreg.	empreg./mil lig.	percentual	R$/ kWh
IN003	IN026	IN027	IN004	IN005	IN006	IN012	IN029	IN007	IN008	IN030	IN031	IN032	IN033	IN034	IN035	IN036	IN037	IN038	IN039	IN040	IN041	IN042	IN054	IN018	IN019	IN102	IN048	IN002	IN045	IN101	IN060
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	IntegerField	IntegerField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


INDICADORES OPERACIONAIS  - ÁGUA
Índice de atendimento total de água	Índice de atendimento urbano de água	Densidade de economias de água por ligação	Participação das economias residenciais de água no total das economias de água	Índice de macromedição	Índice de hidrometração	Índice de micromedição relativo ao volume disponibilizado	Índice de micromedição relativo ao consumo	Índice de fluoretação de água	Índice de consumo de água	Volume de água disponibilizado por economia	Consumo médio de água por economia	Consumo micromedido por economia	Consumo de água faturado por economia	Consumo médio per Capita de água	Índice de consumo de energia elétrica em sistemas de abastecimento de água	Extensão da rede de água por ligação	Índice de faturamento de água	Índice de perdas faturamento	Índice de perdas na distribuição	Índice bruto de perdas lineares	Índice de perdas por ligação
percentual	percentual	econ./lig.	percentual	percentual	percentual	percentual	percentual	percentual	percentual	m³/mês/econ	m³/mês/econ	m³/mês/econ	m³/mês/econ	l/hab.dia	kWh/m3	m/lig.	percentual	percentual	percentual	m³/dia/km	l/dia/lig.
IN055	IN023	IN001	IN043	IN011	IN009	IN010	IN044	IN057	IN052	IN025	IN053	IN014	IN017	IN022	IN058	IN020	IN028	IN013	IN049	IN050	IN051
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


INDICADORES OPERACIONAIS  - ESGOTO
Índice de atendimento total de esgoto referido aos municípios atendidos com água	Índice de atendimento urbano de esgoto referido aos municípios atendidos com água	Índice de atendimento urbano de esgoto referido aos municípios atendidos com esgoto	Índice de coleta de esgoto	Índice de tratamento de esgoto	Índice de esgoto tratado referido à água consumida	Extensão da rede de esgoto por ligação	Índice de consumo de energia elétrica em sistemas de esgotamento sanitário
percentual	percentual	percentual	percentual	percentual	percentual	m/lig.	kWh/m³
IN056	IN024	IN047	IN015	IN016	IN046	IN021	IN059
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


INDICADORES SOBRE QUALIDADE
Economias atingidas por paralisações	Duração média das paralisações	Economias atingidas por intermitências	Duração média das intermitências	Duração média dos reparos de extravasamentos de esgotos	Extravasamentos de esgotos por extensão de rede	Duração média dos serviços executados	Índice de conformidade da quantidade de amostra - Cloro Residual	Incidência das análises de cloro residual fora do padrão	Índice de conformidade da quantidade de amostra - Turbidez	Incidência das análises de turbidez fora do padrão	Índice de conformidade da quantidade de amostra - Coliformes Totais	Incidência das análises de coliformes totais fora do padrão
econ./paralis.	horas/paralis.	econ./interrup.	horas/interrup.	horas/extrav.	extrav./km	hora/serviço	percentual	percentual	percentual	percentual	percentual	percentual
IN071	IN072	IN073	IN074	IN077	IN082	IN083	IN079	IN075	IN080	IN076	IN085	IN084
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField


INDICADORES DE BALANÇO CONTÁBIL
Liquidez corrente	Liquidez geral	Grau de endividamento	Margem operacional com depreciação	Margem operacional sem depreciação	Margem líquida com depreciação	Margem líquida sem depreciação	Retorno sobre o patrimônio líquido	Composição de exigibilidades
			percentual	percentual	percentual	percentual	percentual	percentual
IN061	IN062	IN063	IN064	IN068	IN065	IN069	IN066	IN067
DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField	DecimalField'''


s = u'''Dados gerais
Nome da Secretaria ou Setor responsável pelos serviços de Drenagem e Manejo das Águas Pluviais Urbanas no município	Região Hidrográfica em que se encontra o município (Fonte: ANA):	Nome da(s) bacia(s) hidrográfica(s) a que pertence o município (Fonte: ANA):	Comitê de Bacia ou de Sub-bacia Hidrográfica

nome_scretaria	regiao_hidrografica	bacias_hidrograficas	comite_bacias
CharField	CharField	CharField	CharField


Dados da Cobrança
Forma de cobrança ou de ônus indireto pelo uso ou disposição dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Quantidade total de unidades edificadas urbanas tributadas com taxa específica dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Valor da taxa específica dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas por unidade edificada urbana

forma_cobranca	qtd_unidades	valor_taxa
CharField	IntegerField	DecimalField


Dados financeiros
Quantidade de pessoal próprio alocado nos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Quantidade de pessoal terceirizado alocado nos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Formas de custeio dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Receita total dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Despesas de Exploração (DEX) diretas ou de custeio totais dos serviços de Drenagem e Manejo das Águas Pluviais Urbanas	Desembolsos de investimentos com recursos próprios em Drenagem e Manejo das Águas Pluviais Urbanas 	Investimentos com recursos onerosos em Drenagem e Manejo das Águas Pluviais Urbanas	Investimentos com recursos não onerosos em Drenagem e Manejo das Águas Pluviais Urbanas

qtd_pessoal_proprio	qtd_pessoal_tercerizado	forma_custeio	receita_total	despesa_exploracao_direta	desenbolso_proprio	investimento_recursos_onerosos	investimento_recursos_nao_onerosos
IntegerField	IntegerField	CharField	DecimalField	DecimalField	CharField	DecimalField	DecimalField


Dados de infraestrutura
Tipo de sistema de Drenagem Urbana	Extensão total de vias públicas urbanas do município	Extensão total de vias públicas urbanas com pavimento e meio-fio	Quantidade de bocas de lobo existentes no município	Quantidade de poços de visita (PV) existentes no município	Extensão total de vias públicas urbanas com redes ou canais de águas pluviais subterrâneos	Extensão total dos cursos d'água naturais em áreas urbanas	Extensão total de parques lineares ao longo de cursos d’água	área de Parques lineares
	Km	Km			Km	Km	Km	Km2
tipo_sistema	extensao_total	extensao_total_pavimentada	qtd_bocas_lobo	qtd_pocos	extensao_vias_publicas	extensao_cursos_de_agua	extensao_parques_lineares	area_parques_lineares
CharField	DecimalField	DecimalField	IntegerField	IntegerField	DecimalField	DecimalField	DecimalField	Decimal


Dados operacionais
Limpeza em metro linear das estruturas de drenagem do Município	Existem instrumentos de controle e monitoramento hidrológicos	Existem sistemas de alerta de riscos hidrológicos	Existe mapeamento de áreas de risco de inundação dos cursos d'água urbanos	Quantidade de domicílios sujeitos a risco de inundação	Número de alagamentos na área urbana do município 	Número de unidades edificadas atingidas na área urbana do município devido a eventos hidrológicos impactantes

qtd_metros_limpesa	existe_instrumento_controle	existe_sistema_alerta	existe_mapeamento_risco	qtd_domicilios_risco_iinundacao	numero_alagamentos	numero_unidades_edificadas
IntegerField	NullBooleanField	NullBooleanField	NullBooleanField	IntegerField	IntegerField	IntegerField'''


class Command(BaseCommand):
    def handle(self, *args, **options):
        classes = []
        for bloco in s.split('\n\n\n'):
            tokens = bloco.split('\n')
            if len(tokens) != 5:
                class_name = tokens[0]
                fieldset1 = tokens[1:6]
                fieldset2 = tokens[7:]
                fieldset_name1, field_labels1, help_texts1, field_names1, field_types1 = fieldset1
                fieldset_name2, field_labels2, help_texts2, field_names2, field_types2 = fieldset2

                field_labels = '%s\t%s' % (field_labels1, field_labels2)
                help_texts = '%s\t%s' % (help_texts1, help_texts2)
                field_names = '%s\t%s' % (field_names1, field_names2)
                field_types = '%s\t%s' % (field_types1, field_types2)

                field_labels = field_labels.split('\t')
                help_texts = help_texts.split('\t')
                field_names = field_names.lower().split('\t')
                field_types = field_types.split('\t')
                fields = []
                for i in range(0, len(field_labels)):
                    fields.append(
                        dict(label=field_labels[i], help_text=help_texts and help_texts[i] or '', name=field_names[i],
                             type=field_types[i]))
                name = 'Formulario%s' % unicode(len(classes) + 18).rjust(3, '0')
                fieldsets = []
                fieldset_names1 = []
                for x in field_names1.split('\t'):
                    fieldset_names1.append("'%s'" % x.lower())
                fieldsets.append(dict(name=fieldset_name1, fields=', '.join(fieldset_names1)))
                fieldset_names2 = []
                for x in field_names2.split('\t'):
                    fieldset_names2.append("'%s'" % x.lower())
                fieldsets.append(dict(name=fieldset_name2, fields=', '.join(fieldset_names2)))
                classes.append(dict(name=name, class_name=class_name, fields=fields, fieldsets=fieldsets))

            else:
                fieldset_name, field_labels, help_texts, field_names, field_types = tokens
                class_name = normalizar(fieldset_name)

                fieldset_names = []
                for x in field_names.split('\t'):
                    fieldset_names.append("'%s'" % x.lower())
                fieldsets = [dict(name='Dados Gerais', fields=', '.join(fieldset_names))]

                field_labels = field_labels.split('\t')
                help_texts = help_texts.strip() and help_texts.split('\t') or None
                field_names = field_names.lower().split('\t')
                field_types = field_types.split('\t')
                fields = []
                for i in range(0, len(field_labels)):
                    fields.append(dict(label=field_labels[i], help_text=help_texts and help_texts[i] or '', name=field_names[i], type=field_types[i]))
                name = 'Formulario%s' % unicode(len(classes)+18).rjust(3, '0')

                classes.append(dict(name=name, class_name=class_name, fields=fields, fieldsets=fieldsets))
        print render_to_string('classe.txt', dict(classes=classes))



