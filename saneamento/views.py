# -*- coding: utf-8 -*-
import datetime
from djangoplus.ui.breadcrumbs import httprr
from django.apps import apps
from saneamento.models import *
from saneamento.forms import *
from djangoplus.decorators.views import view, action


@view(u'Gerar Relatório de Acompanhamento', login_required=False, style='ajax pdf', menu=u'Relatório::Gerar Relatório de Acompanhamento', icon='fa-bar-chart')
def relatorio(request):
    hoje = datetime.date.today()
    registros = []
    for i in range(1, 22):
        model_name = 'Formulario%s' % unicode(i).rjust(3, '0')
        model = apps.get_model('saneamento', model_name)
        dados = []
        qs = model.objects.filter(ano=hoje.year)
        if not qs.exists():
            model.objects.create(ano=hoje.year)
        for obj in qs.all():
            for field in model._meta.fields:
                if field.name not in ('id', 'ano'):
                    valor = getattr(obj, field.name)
                    dados.append(dict(chave=field.verbose_name, valor=valor))
            registros.append(dict(titulo=model._meta.verbose_name, dados=dados))
    return locals()



